using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // List of Transfroms for Enemy Spawnpoint Objects.
    public List<Transform> FlySpawnPointObjects;
    public List<Transform> WalkSpawnPointObjects;
    public List<Transform> SawBladeSpawnPointObjects;
    public List<Transform> StalaktitSpawnPointObjects;
    public List<Transform> VulcanSpawnPointObjects;

    // List of GameObject for Enemy Prefabs.
    public List<GameObject> FlyEnemiesToSpawn;
    public List<GameObject> WalkEnemiesToSpawn;
    public List<GameObject> SawBladeEnemiesToSpawn;
    public List<GameObject> StalaktitEnemiesToSpawn;
    public List<GameObject> VulcanEnemiesToSpawn;

    // Numbers of Enemies to spawn.
    public int NumberToSpawn;

    private GameObject room;
    private ResourceManager resourceManager;
    void Start()
    {
        resourceManager = ResourceManager.GetResourceManager();

        FlySpawnPointObjects = new List<Transform>();
        WalkSpawnPointObjects = new List<Transform>();
        SawBladeSpawnPointObjects = new List<Transform>();
        StalaktitSpawnPointObjects = new List<Transform>();
        VulcanSpawnPointObjects = new List<Transform>();

        FlyEnemiesToSpawn = new List<GameObject>();
        WalkEnemiesToSpawn = new List<GameObject>();
        SawBladeEnemiesToSpawn = new List<GameObject>();
        StalaktitEnemiesToSpawn = new List<GameObject>();
        VulcanEnemiesToSpawn = new List<GameObject>();

        // Set the Room or Parent object of the Spawnpoints!
        room = GameObject.Find("Test");

        // Prepare Enemy SpawnPoint(s) Arrays, if the Spawnpoint(s) exists in the Scene the Methode to spawn is called.
        if (room.transform.Find("FlySpawnPoint(s)") != null)
        {
            FlySpawnPointObjects.AddRange(room.transform.Find("FlySpawnPoint(s)").GetComponentsInChildren<Transform>());
            FlySpawnPointObjects.RemoveAt(0);
            SpawnFlyingEnemies();
        }
        if (room.transform.Find("WalkSpawnPoint(s)") != null)
        {
            WalkSpawnPointObjects.AddRange(room.transform.Find("WalkSpawnPoint(s)").GetComponentsInChildren<Transform>());
            WalkSpawnPointObjects.RemoveAt(0);
            SpawnWalkingEnemies();
        }
        if (room.transform.Find("SawBladeSpawnPoint(s)") != null)
        {
            SawBladeSpawnPointObjects.AddRange(room.transform.Find("SawBladeSpawnPoint(s)").GetComponentsInChildren<Transform>());
            SawBladeSpawnPointObjects.RemoveAt(0);
            SpawnSawBladeEnemies();
        }
        if (room.transform.Find("StalaktitSpawnPoint(s)") != null)
        {
            StalaktitSpawnPointObjects.AddRange(room.transform.Find("StalaktitSpawnPoint(s)").GetComponentsInChildren<Transform>());
            StalaktitSpawnPointObjects.RemoveAt(0);
            SpawnStalaktitEnemies();
        }
        if (room.transform.Find("VulcanSpawnPoint(s)") != null)
        {
            VulcanSpawnPointObjects.AddRange(room.transform.Find("VulcanSpawnPoint(s)").GetComponentsInChildren<Transform>());
            VulcanSpawnPointObjects.RemoveAt(0);
            SpawnVulcanEnemies();
        }
    }
    private void SpawnFlyingEnemies()
    {
        if (FlySpawnPointObjects != null)
        {
            string[] FlyingEnemyArray = {
                                          "FlyingEnemy/FlyingEnemyLvl1",
                                          "FlyingEnemy/FlyingEnemyLvl2",
                                          "FlyingEnemy/FlyingEnemyLvl3"
                                         };

            if (FlyingEnemyArray != null)
            {
                for (int i = 0; i < FlyingEnemyArray.Length; i++)
                {
                    resourceManager.LoadAsset("Prefab/Enemies/" + FlyingEnemyArray[i]);
                    FlyEnemiesToSpawn.Add(resourceManager.GetAssetByPath<GameObject>("Prefab/Enemies/" + FlyingEnemyArray[i]));
                }
            }

            for (int i = 0; i < FlySpawnPointObjects.Count; i++)
            {
                Instantiate(FlyEnemiesToSpawn[Random.Range(0, FlyEnemiesToSpawn.Count)], FlySpawnPointObjects[i].transform.position, FlySpawnPointObjects[i].transform.rotation);
            }
        }
    }

    private void SpawnWalkingEnemies()
    {
        if (WalkSpawnPointObjects != null)
        {
            string[] MovingEnemyArray = {
                                          "MovingEnemy/MovingEnemyLvl1",
                                          "MovingEnemy/MovingEnemyLvl2",
                                        };
            if (MovingEnemyArray != null)
            {
                for (int i = 0; i < MovingEnemyArray.Length; i++)
                {
                    resourceManager.LoadAsset("Prefab/Enemies/" + MovingEnemyArray[i]);
                    WalkEnemiesToSpawn.Add(resourceManager.GetAssetByPath<GameObject>("Prefab/Enemies/" + MovingEnemyArray[i]));
                }
            }

            for (int i = 0; i < WalkSpawnPointObjects.Count; i++)
            {
                Instantiate(WalkEnemiesToSpawn[Random.Range(0, WalkEnemiesToSpawn.Count)], WalkSpawnPointObjects[i].transform.position, WalkSpawnPointObjects[i].transform.rotation);
            }
        }
    }

    private void SpawnSawBladeEnemies()
    {
        if (SawBladeSpawnPointObjects != null)
        {
            string SawBlade = "SawBlade/SawBlade";
            if (SawBlade != null)
            {
                resourceManager.LoadAsset("Prefab/Enemies/" + SawBlade);
                SawBladeEnemiesToSpawn.Add(resourceManager.GetAssetByPath<GameObject>("Prefab/Enemies/" + SawBlade));
            }

            for (int i = 0; i < SawBladeSpawnPointObjects.Count; i++)
            {
                Instantiate(SawBladeEnemiesToSpawn[Random.Range(0, SawBladeEnemiesToSpawn.Count)], SawBladeSpawnPointObjects[i].transform.position, SawBladeSpawnPointObjects[i].transform.rotation);
            }
        }
    }

    private void SpawnStalaktitEnemies()
    {
        if (StalaktitSpawnPointObjects != null)
        {
            string Stalaktit = "Stalaktit/Stalaktit";
            if (Stalaktit != null)
            {
                resourceManager.LoadAsset("Prefab/Enemies/" + Stalaktit);
                StalaktitEnemiesToSpawn.Add(resourceManager.GetAssetByPath<GameObject>("Prefab/Enemies/" + Stalaktit));
            }

            for (int i = 0; i < StalaktitSpawnPointObjects.Count; i++)
            {
                Instantiate(StalaktitEnemiesToSpawn[Random.Range(0, StalaktitEnemiesToSpawn.Count)], StalaktitSpawnPointObjects[i].transform.position, StalaktitSpawnPointObjects[i].transform.rotation);
            }
        }
    }

    private void SpawnVulcanEnemies()
    {
        if (VulcanSpawnPointObjects != null)
        {
            string Vulcan = "Vulcan/Vulcan";
            if (Vulcan != null)
            {
                resourceManager.LoadAsset("Prefab/Enemies/" + Vulcan);
                VulcanEnemiesToSpawn.Add(resourceManager.GetAssetByPath<GameObject>("Prefab/Enemies/" + Vulcan));
            }

            for (int i = 0; i < VulcanSpawnPointObjects.Count; i++)
            {
                Instantiate(VulcanEnemiesToSpawn[Random.Range(0, VulcanEnemiesToSpawn.Count)], VulcanSpawnPointObjects[i].transform.position, VulcanSpawnPointObjects[i].transform.rotation);
            }
        }
    }
}
