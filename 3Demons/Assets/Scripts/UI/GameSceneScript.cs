using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSceneScript : MonoBehaviour
{
    GameManager gm;
    public Text tutorialTextObject;
    public Text weaponText;
    public Text currentHealth;
    public Text ammoCooldown;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.GetGameManager();
        tutorialTextObject.text = "Controls: " + "\n" +
                                  "Up: " + KeyCode.W.ToString() + "\n" +
                                  "Down: " + KeyCode.S.ToString() + "\n" +
                                  "Left: " + KeyCode.A.ToString() + "\n" +
                                  "Right: " + KeyCode.D.ToString() + "\n" +
                                   "Jump: " + KeyCode.Space.ToString() + "\n" +
                                   "Bomb: " + KeyCode.D.ToString() + "\n" + "\n" +
                                   "Press T to close and show the Tutorial again.";
    }
    
    // Update is called once per frame
    void Update()
    {
        RefreshUI();
        if (Input.GetKeyDown(KeyCode.T))
        {
            tutorialTextObject.gameObject.SetActive(!tutorialTextObject.gameObject.activeSelf);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {

            GameManager.SceneSwitch("MenuScene");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void RefreshUI()
    {
        if (gm != null && gm.playerScript != null)
        {
            currentHealth.text = gm.playerScript.currentHealth.ToString();
        }
        else
        {
            currentHealth.text = "XX";
        }
    }
}
