using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private GameManager gameManager;

    public CharacterController characterController;
    public Rigidbody playerRB;


    [Header("Speeds")]
    public float movingSpeed = 12f;
    public float gravity = -15f;
    public float jumpHeight = 3f; // this must always be higher than the gravity
    public float climbSpeed = 5f;
    public float playerSpeed = 6f;


    [Header("Health")]
    public int currentHealth;
    private bool invincible = false;
    private float invincibleTime = 0;


    [Header("Checks")]
    public Transform groundCheck;
    public float groundDistance = 0.1f;
    public LayerMask groundMask;
    public bool canMove = true;

    //Ladder related variables
    private bool isLadder;
    private bool isClimbing;
    


    private Vector3 velocity;
    private bool isGrounded;


    [Header("Collectabiles")]
    public int LifePoints;
    public int CoinValue;
    public int EnergiePoints;
    public int AreaOfEffect;
    public int ItemValue;


    //player ./item/usables
    [Header("Usables")]
    public int normalKey;
    public int specialKey;
    public int bombe;
    public int openedChest;

    private bool usablesCollectiblesKeyCodePressed = false;


    void Start()
    {
        gameManager = GameManager.GetGameManager();
        gameManager.Player = gameObject;
        gameManager.playerScript = this;
        if (invincibleTime <= 0)
        {
            invincible = false;
        }
        invincibleTime -= Time.deltaTime;
        //currentHealth = gameManager.playerData.maxHealth;
    }


    void Update()
    {
        if (!canMove) return;

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        // get the movement on x axis (horizontal)
        float positionHorizontal = Input.GetAxis("Horizontal");


        // calculate movement speed
        Vector3 movement = transform.right * positionHorizontal;
        characterController.Move(movement * movingSpeed * Time.deltaTime);


        // hold the character on the ground if he is grounded
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }


        // handle jumping if space is pressed AND the user is currently on the ground
        if (Input.GetKey(KeycodeManager.GetKeyCodeByAction("jump")) && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }


        // handle ladder movement
        float positionVertical = Input.GetAxisRaw("Vertical");
        if (isLadder && Mathf.Abs(positionVertical) > 0f)
        {
            isClimbing = true;
        }


        usablesCollectiblesKeyCodePressed = Input.GetKeyDown(KeycodeManager.GetKeyCodeByAction("interact"));


        if (isClimbing)
        {
            // Player is connected to a ladder!
            bool playerClimbingUp = Input.GetKey(KeycodeManager.GetKeyCodeByAction("ladderUp"));
            bool playerClimbingDown = Input.GetKey(KeycodeManager.GetKeyCodeByAction("ladderDown"));

            if (playerClimbingDown || playerClimbingUp)
            {
                // if player is currently climbing, move controller

                velocity.y = Mathf.Sqrt(climbSpeed * 100 * Time.deltaTime);

                // invert climbing direction if player is climbing down
                if (playerClimbingDown)
                    velocity.y *= -1;
            }
            else
            {
                // player is idling on a ladder, disabled every y axis movement
                velocity.y = 0;
            }
        }
        else
        {
            // if player is not at ladder, apply gravity
            velocity.y += gravity * Time.deltaTime;
        }


        // apply the velocity to the characterController
        characterController.Move(velocity * Time.deltaTime);
    }

    /// <summary>
    /// Collecting items by collision
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter(Collision collision)
    {
        if (Input.GetKey(KeycodeManager.GetKeyCodeByAction("interact")))
        {
            //PREFAB_Sammel_Methode
        }
    }

    /// <summary>
    /// Climbing ladder - TriggerEnter
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag.ToString())
        {
            case ("Collectibles"):
                Debug.Log(other.tag.ToString());
                Collectibles c1 = other.gameObject.GetComponent<Collectibles>();
                CoinValue += c1.coinValue;
                currentHealth += c1.lifePoints;
                EnergiePoints += c1.energiePoints;
                AreaOfEffect += c1.areaOfEffect;
                ItemValue += c1.itemValue;
                Destroy(other.gameObject);
                break;
            case ("Usables"):
                Debug.Log(other.tag.ToString());
                Usables u1 = other.gameObject.GetComponent<Usables>();
                normalKey += u1.normal_key;
                specialKey += u1.special_Key;
                bombe += u1.bombe;
                Destroy(other.gameObject);
                break;
            case ("Ladder"):
                isLadder = true;
                break;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!usablesCollectiblesKeyCodePressed) return;

        switch (other.gameObject.tag.ToString())
        {
            case ("T�r"):
                if (specialKey > 0)
                {
                    //other = T�r �ffnen Methode
                }
                break;
            case ("Bombe"):
                if (bombe > 0)
                {
                    //other = Bombe Legen Methode
                }
                break;
            case ("Chest"):
                if (normalKey > 0)
                {
                    //other = Kiste �ffnen Methode
                    openedChest++;
                    Destroy(other.gameObject);
                    normalKey--;
                }
                break;
        }
    }

    /// <summary>
    /// Climbing ladder - TriggerExit
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ladder"))
        {
            isLadder = false;
            isClimbing = false;
        }
    }

    /// <summary>
    /// Inflicting damage to player
    /// </summary>
    /// <param name="dmg"></param>
    public void GetDamage(int dmg)
    {
        if (!invincible)
        {
            currentHealth -= dmg;
            invincible = true;
            invincibleTime = 2;
        }
    }

    public IEnumerator Knockback(float knockDur, float knockbackPwr, Vector3 knockbackDir)
    {
        float timer = 0;
        while (knockDur > timer)
        {
            timer += Time.deltaTime;
            playerRB.AddForce(new Vector3(knockbackDir.x * -100, knockbackDir.y * knockbackPwr, transform.position.z));
        }
        yield return 0;
    }
}
