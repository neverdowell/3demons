// #define TEST

using System.Collections.Generic;
using System;
using UnityEngine;

//Made by Jan Schürkamp
public class ResourceManager : MonoBehaviour
{
    private static ResourceManager ResourceManagerInstance;

    private Dictionary<string, UnityEngine.Object> _Assets;

    /// <summary>
    /// Private constructor
    /// </summary>
    private ResourceManager()
    {
    }

    /// <summary>
    /// Called on Awake by Unity.
    /// </summary>
    public void Awake()
    {
        if(ResourceManagerInstance == null)
        {
            ResourceManagerInstance = this;
            _Assets = new Dictionary<string, UnityEngine.Object>();
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if(ResourceManagerInstance != this)
                Destroy(this.gameObject);
        }
    }

#if TEST
    public void Start()
    {
        //Testing for ResourceManager
        //Load Assets
        if(!GetResMgr().LoadAsset("TestCube"))
            Debug.LogError("[ResourceManager] Unable to load TestCube from Resources folder.!");

        //Get Assets
        GameObject cubeObjPath = GetResMgr().GetAssetByPath<GameObject>("TestCube");
        if(cubeObjPath == null)
            Debug.LogError("[ResourceManager] Unable to get TestCube from Resources folder by path!");

        //Instantiate Assets
        GameObject obj = Instantiate<GameObject>(cubeObjPath);
        obj.transform.localScale = new Vector3(1, 2, 1);

        //Unload test
        GetResMgr().LoadAsset("TestMat");
        Material mat = GetResMgr().GetAssetByPath<Material>("TestMat");
        GetResMgr().UnloadAsset("TestMat.material");
    }
#endif

    /// <summary>
    /// Loads a resource from the Resources folder.
    ///
    /// Note: Path is relative from Resources folder.
    ///       Path is case insensitive.
    ///       Must be unique (no duplicates!)
    ///       Can't contain file endings
    ///       Must only use forward slashes
    /// <summary>
    /// <param name="path">Path to resource.</param>
    /// <returns>True on successful loading or already loaded, false otherwise.</returns>
    public bool LoadAsset(string path)
    {
        path = SanitizePath(path);

        if(_Assets.ContainsKey(path))
            return true;

        UnityEngine.Object resource = Resources.Load(path);
        if(resource == null)
            return false;

        _Assets.Add(path, resource);
        return true;
    }

    /// <summary>
    /// Unloads a loaded resource.
    ///
    /// Note: Do not use to unload Gameobjects/Prefabs!
    /// <summary>
    /// <param name="path">Path to resource.</param>
    public void UnloadAsset(string path)
    {
        path = SanitizePath(path);

        if(!_Assets.ContainsKey(path))
            return;

        Resources.UnloadAsset(_Assets[path]);
        _Assets.Remove(path);
    }

    /// <summary>
    /// Retrieve a resource via its path.
    /// <summary>
    /// <typeparam name="T">Type of resource.</typeparam>
    /// <param name="path">Path of resource.</param>
    /// <returns>Resource converted to type T on success, default constructed T otherwise.</returns>
    public T GetAssetByPath<T>(string path)
    {
        path = SanitizePath(path);

        if(!LoadAsset(path))
            return default(T);

        return (T)Convert.ChangeType(_Assets[path], typeof(T));
    }

    private string SanitizePath(string path)
    {
        //Path is case insensitive
        //Must start from Resources folder
        //Must be unique
        //Cannot contain file endings
        //Must only use forward slashes

        //Remove everything before "Resources/"
        int resourceIndex = path.IndexOf("Resources/");
        if(resourceIndex != -1)
            path = path.Substring(resourceIndex + 10);

        //Remove file endings
        int endingIndex = path.LastIndexOf('.');
        if(endingIndex != -1)
            path = path.Substring(0, endingIndex);

        //Convert back slashes to forward slashes
        path = path.Replace('\\', '/');

        return path;
    }

    /// <summary>
    /// Get the singleton ResourceManager instance
    /// </summary>
    /// <returns>ResourceManager instance</returns>
    [Obsolete("To be removed. Use GetResourceManager() instead.")]
    public static ResourceManager GetResMgr()
    {
        return GetResourceManager();
    }

    /// <summary>
    /// Get the singleton ResourceManager instance
    /// </summary>
    /// <returns>ResourceManager instance</returns>
    public static ResourceManager GetResourceManager()
    {
        return ResourceManagerInstance;
    }
}
