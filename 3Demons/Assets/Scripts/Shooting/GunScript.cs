﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    public Rigidbody rb;
    public Camera cam;
    public GameObject currentWeapon;
    public GameObject[] weaponGameObjects;
    public ScriptableObjectWeapon[] weaponDatas;
    public int selectedWeapon;
    public Transform GunMuzzlePistole;
    //public GameObject Bullet;
    public int index;
    //public GameObject BulletRifle;


    public GameObject BulletPistole;


    public bool[] weaponCooldowns = {false, false};


    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        //Funktioniert

        weaponGameObjects[0].gameObject.SetActive(true);
        if (weaponGameObjects[1] != null){
            weaponGameObjects[1].gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Funktioniert
        Vector2 positionOnScreen = cam.WorldToViewportPoint(transform.position);
        Vector2 mouseOnScreen = (Vector2)cam.ScreenToViewportPoint(Input.mousePosition);
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));

        //SwitchWeapons();

        //shooting funktioniert nur Rifle -> Cooldown funktioniert bei beiden nicht
        if (Input.GetButtonDown("Fire1"))
        {
            if (!weaponCooldowns[0] && weaponDatas[0] != null)
            {
                Shoot(0);
                weaponCooldowns[0] = true;
                Invoke("ResetCooldown0", weaponDatas[0].cooldown);
            }

            

        } else if (Input.GetButtonDown("Fire2")){
        if (!weaponCooldowns[1] && weaponDatas[1] != null)
            {
                Shoot(1);
                weaponCooldowns[1] = true;
                Invoke("ResetCooldown1", weaponDatas[1].cooldown);
            }
        }
    }
        //Funktioniert nicht?!
        public void ResetCooldown0()
        {
            weaponCooldowns[0] = false;
        }

        //Funktioniert nicht?!
        public void ResetCooldown1()
        {
            weaponCooldowns[1] = false;
        }

        //Auf eine Funktion kürzen
        void Shoot(int weaponIndex)
        {
            //Create Bullet pistole
            GameObject bullet = Instantiate(weaponDatas[weaponIndex].bullet, GunMuzzlePistole.position, GunMuzzlePistole.rotation);
            Rigidbody rbBullet = bullet.GetComponent<Rigidbody>();
            //Make the Bullet fly
            rbBullet.AddForce(GunMuzzlePistole.up * weaponDatas[weaponIndex].range, ForceMode.Impulse);
        }

        //void Shootrifle()
        //{
        //    //Create bullet Rifle
        //    GameObject bulletRifle = Instantiate(BulletRifle, GunMuzzleRifle.position, GunMuzzleRifle.rotation);
        //    Rigidbody rbRifle = bulletRifle.GetComponent<Rigidbody>();
        //    //Make the Bullet fly
        //    rbRifle.AddForce(GunMuzzleRifle.up * bulletForceRifle, ForceMode.Impulse);
        //}

        //if (Input.GetKeyDown(KeyCode.Alpha1))
        //{
        //    for (int i = 0; i < weaponDatas.Length; i++)
        //    {
        //        weaponDatas[i].gameObject.SetActive(i == index);
        //    }

        //}
        //if (Input.GetKeyDown(KeyCode.Alpha2))
        //{
        //    for (int i = 0; i > weaponDatas.Length; i++)
        //    {
        //        weaponDatas[i].gameObject.SetActive(i == index);
        //    }
        //}


    //Funktioniert
    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg - 180f;
    }

   
    //Funktioniert nicht mehr 
    void SwitchWeapons(int newIndex)
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {

            for (int i = 0; i < weaponGameObjects.Length; i++)
            {
                weaponGameObjects[i].gameObject.SetActive(i == index);
            }

            for (int i = 0; i < weaponGameObjects.Length; i++)
            {
                weaponGameObjects[i].SetActive(false);
            }
            weaponGameObjects[index].SetActive(true);

            currentWeapon = weaponGameObjects[index];

            for (int i = 0; i > weaponGameObjects.Length; i++)
            {
                weaponGameObjects[i].SetActive(false);
            }
            weaponGameObjects[newIndex].SetActive(true);

            currentWeapon = weaponGameObjects[newIndex];
        }
        //    weaponDatas[1].gameObject.SetActive(false);
        //    currentWeapon = weaponDatas[0];
        //}
        //else if (Input.GetKeyDown(KeyCode.Alpha2))
        //{
        //    weaponDatas[1].gameObject.SetActive(true);
        //    weaponDatas[0].gameObject.SetActive(false);
        //    currentWeapon = weaponDatas[1];
    }

    //    for (int i = 0; i < weaponDatas.Length; i++)
    //    {
    //        weaponDatas[i].gameObject.SetActive(i == index);
    //    }

    //    for (int i = 0; i < weaponDatas.Length; i++)
    //    {
    //        weaponDatas[i].SetActive(false);
    //    }
    //    weaponDatas[index].SetActive(true);

    //    currentWeapon = weaponDatas[index];

    //    for (int i = 0; i < weaponDatas.Length; i++)
    //    {
    //        weaponDatas[i].SetActive(false);
    //    }
    //    weaponDatas[newIndex].SetActive(true);

    //    currentWeapon = weaponDatas[newIndex];
    //}

}

