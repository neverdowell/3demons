using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

//public enum Enemy
//{
//    Stalaktit,
//    Vulcan,
//    Beartrap,
//    Shooter,
//    Sawblade,
//    Knight,
//    Bullet,
//    LavaBall,
//    SpikeBall,
//    Hornet,
//    Bat
//}
public class EnemyController : MonoBehaviour
{
    public ScriptableEnemy scriptableEnemy;
    //public Enemy enemy;
    private GameManager gameManager;
    private int currentHealth;
    private bool canMove;
    private Vector3 startPosition;
    private bool changedWay;
    private Rigidbody rigidbody;
    private CharacterController characterController;
    private float bulletCooldown;
    private int positionIndex = 0;
    RaycastHit raycastHit;

    // Start is called before the first frame update
    void Start()
    {
        if (scriptableEnemy == null)
        {
            Debug.LogError($"Bitte f�gen Sie ein \"Scriptable Object\" hinzu");
            Destroy(gameObject);
        }
        gameManager = GameManager.GetGameManager();
        currentHealth = scriptableEnemy.maxHealth;
        canMove = scriptableEnemy.canMove;
        startPosition = transform.position;
        bulletCooldown = scriptableEnemy.bulletCooldown;
        changedWay = false;
        if (gameObject.GetComponent<Rigidbody>() == null)
        {
            rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.useGravity = scriptableEnemy.hasGravity;
            rigidbody.mass = 1.55f;
            if (scriptableEnemy.patrollingMovement)
            {
                rigidbody.isKinematic = true;
            }
        }
        else
        {
            rigidbody = gameObject.GetComponent<Rigidbody>();
        }
        if (gameObject.GetComponent<CharacterController>() == null)
        {
            characterController = gameObject.AddComponent<CharacterController>();
            if (!scriptableEnemy.patrollingMovement)
            {
                characterController.enabled = false;
            }
        }
        else
        {
            characterController = gameObject.GetComponent<CharacterController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Action();
        Movement();
        UpdateCooldowns();
        ControllCollision();
    }
    /// <summary>
    /// When this object collides with an other object.
    /// </summary>
    /// <param name="collision">collided object</param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name == gameManager.playerTag)
        {
            gameManager.playerScript.GetDamage(scriptableEnemy.damage);
            Debug.Log($"{scriptableEnemy.name} hat den Spieler angegriffen, damage= {scriptableEnemy.damage}.");
            if (scriptableEnemy.isBullet && !scriptableEnemy.invincible)
            {
                Death();
            }
        }
        if (!scriptableEnemy.invincible)
        {
            if (collision.gameObject.TryGetComponent<EnemyController>(out EnemyController controller))
            {
                if (controller.scriptableEnemy.friendlyFireOn)
                {
                    Death();
                }
            }
            else if (scriptableEnemy.isBullet)
            {
                Death();
            }
        }
    }
    /// <summary>
    /// Triggers the enemy when the player moves under this object.
    /// </summary>
    /// <returns>Is player under enemy</returns>
    private bool TriggerEnemyAbove()
    {
        Ray ray = new Ray(transform.position, -transform.up);
        if (Physics.Raycast(ray, out raycastHit))
        {
            return true;
        }
        return false;
    }
    /// <summary>
    /// Activates a specific action.
    /// </summary>
    private async void Action()
    {
        if (scriptableEnemy.waitsAbovePlayer && TriggerEnemyAbove())
        {
            await Task.Delay(scriptableEnemy.reactionTime);
            canMove = true;
            Debug.Log($"{scriptableEnemy.name} wurde getriggert.");
            Debug.Log($"{scriptableEnemy.name} hatte ein Delay von {(float)scriptableEnemy.reactionTime / 1000} Sekunden");
        }
        if (scriptableEnemy.canShoot && scriptableEnemy.scriptableBullet != null && bulletCooldown <= 0)
        {
            bulletCooldown = scriptableEnemy.bulletCooldown;
            Debug.Log($"{scriptableEnemy.name} hat {scriptableEnemy.scriptableBullet.name} geschossen");
            //gameManager.spawnScript.SpawnProjectile(transform, scriptableEnemy, bullet);
        }
    }
    /// <summary>
    /// Moves the enemy if possible.
    /// </summary>
    private void Movement()
    {
        //Can enemy move
        if (canMove)
        {   //Moves down
            if (scriptableEnemy.downMovementSpeed != 0)
            {
                rigidbody.AddForce(-transform.up * scriptableEnemy.downMovementSpeed);
                canMove = false;
            }
            //Moves forward in parabola path
            if (scriptableEnemy.topMovementSpeed != 0)
            {
                rigidbody.AddForce(transform.up * scriptableEnemy.topMovementSpeed);
                canMove = false;
            }
            if (scriptableEnemy.forwardMovementSpeed != 0)
            {
                rigidbody.AddForce(transform.forward * scriptableEnemy.forwardMovementSpeed);
            }
            if (scriptableEnemy.backwardMovementSpeed != 0)
            {
                rigidbody.AddForce(-transform.forward * scriptableEnemy.backwardMovementSpeed);
            }
            //Moves from a to b
            if (scriptableEnemy.patrollingMovement)
            {
                if (scriptableEnemy.patrollingPositions.Count != 0)
                {
                    var offset = scriptableEnemy.patrollingPositions[positionIndex] - transform.position;
                    if (offset.magnitude > .1f) //maybe mit vector3.distance?
                    {
                        offset = offset.normalized * scriptableEnemy.patrolingMovementSpeed; //muss normalized?
                        characterController.Move(offset * Time.deltaTime);
                        Debug.Log($"Der Gegner l�uft auf den Vektor {scriptableEnemy.patrollingPositions[positionIndex]} zu");
                    }
                    else
                    {
                        if (positionIndex >= scriptableEnemy.patrollingPositions.Count - 1)
                        {
                            positionIndex = 0;
                        }
                        else
                        {
                            positionIndex++;
                        }
                    }
                }
                else
                {
                    characterController.Move(transform.forward * scriptableEnemy.patrolingMovementSpeed * Time.deltaTime);
                    Ray rayDown = new Ray(transform.position + (transform.forward * scriptableEnemy.directionChangeRange), -transform.up);
                    Ray rayForward = new Ray(transform.position , transform.forward);
                    if (!scriptableEnemy.canGoThrewWall && ((!scriptableEnemy.canFly && !Physics.Raycast(rayDown, out raycastHit, scriptableEnemy.vektorDownRange)) || Physics.Raycast(rayForward, out raycastHit, scriptableEnemy.vektorForwardRange)))
                    {
                        //Debug.Log("Drehen " + characterController.transform.localRotation.y);
                        characterController.transform.localRotation *= Quaternion.Euler(0f,  180f, 0f);
                    }
                }
            }
        }
    }
    /// <summary>
    /// Drops Item bevor the enemy dies.
    /// </summary>
    private async void Death()
    {
        Debug.Log($"{scriptableEnemy.name} ist dabei zu sterben.");
        //gameManager.ItemsScript.DropItem(); Droprate einbauen
        if (scriptableEnemy.deathAnimation)
        {
            await Task.Delay(1000);
            Debug.Log($"tot");
        }
        Destroy(gameObject);
    }
    /// <summary>
    /// Lowers all cooldowns.
    /// </summary>
    private void UpdateCooldowns()
    {
        if (bulletCooldown >= 0)
        {
            bulletCooldown -= Time.deltaTime;
        }
    }
    private void ControllCollision()
    {
        //Debug.Log($"Enemy: {scriptableEnemy.name} Character: {characterController.enabled} Rigidbody: {rigidbody.IsSleeping()}");
        if (scriptableEnemy.canGoThrewWall)
        {
            Debug.Log("CHANGE 1");
        }
        else
        {
            Debug.Log("CHANGE 2");
        }
    }
}
