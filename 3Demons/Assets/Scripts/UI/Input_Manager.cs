using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Input_manager : MonoBehaviour
{
    public KeyCode up;
    public KeyCode down;
    public KeyCode walk_left;
    public KeyCode walk_right;
    public KeyCode shoot;
    public KeyCode reload;
    KeyCode[] keyCodes;
    public GameObject input_lbl;
    KeyCode p_key;
    // Start is called before the first frame update
    void Start()
    {
        KeyCode[] tmp = { up, down, walk_left, walk_right, shoot, reload };
        keyCodes = tmp;
        input_lbl.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Event key = Event.current;
        if (key != null && key.isKey && input_lbl.activeSelf)
        {
            p_key = key.keyCode;

            input_lbl.SetActive(false);
        }
    }

    public void UpdateInput(string value)
    {
        Event e = Event.current;
        if (e.isKey && input_lbl.activeSelf)
        {


            for (int i = 0; i < keyCodes.Length; i++)
            {
                if (keyCodes[i].ToString() == value)
                {
                    keyCodes[i] = p_key;
                    Debug.Log(keyCodes[i]);
                }
            }


            //switch (value)
            //{
            //    case "walk_right":

            //        break;
            //    case "walk_left":

            //        break;
            //    case "up":

            //        break;
            //    case "climb_ladder":

            //        break;
            //    case "fall_ladder":

            //        break;
            //    case "shoot":

            //        break;
            //    case "reload":

            //        break;
            //}




        }
    }
}
