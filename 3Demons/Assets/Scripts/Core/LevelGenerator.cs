using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Made by Jan Schürkamp & Jason Farah
public class LevelGenerator : MonoBehaviour
{
    /// <summary>
    /// Types of Rooms.
    /// </summary>
    private enum RoomType
    {
        Unused = 0,
        Normal,
        Start,
        Boss,
        Shop
    }
    /// <summary>
    /// Types of Room Connections.
    /// </summary>
    public enum RoomDoors
    {
        TopLeft = 0,
        TopRight,
        RightTop,
        RightBottom,
        BottomRight,
        BottomLeft,
        LeftBottom,
        LeftTop
    }

    /// <summary>
    /// Enables or Disabled test mode.
    /// </summary>
    public bool TestMode = false;

    /// <summary>
    /// Class containing internal state of Rooms.
    /// </summary>
    private class RoomState
    {
        public RoomState()
        {
            Type = RoomType.Unused;
            Visited = false;
            InsideIndex = -1;
        }

        public RoomType Type;
        public bool Visited;
        public int InsideIndex;
    }

    private GameManager GameManager;
    private ResourceManager ResourceManager;
    private RoomState[,] RoomMap;
    private int NormalRoomInsideCount = -1;

    private int _ActiveRoomX, _ActiveRoomY;
    private int _LastRoomX, _LastRoomY;

    private GameObject _ActiveRoomInside;
    /// <summary>
    /// Property containing the active rooms inside.
    /// </summary>
    public GameObject ActiveRoomInside { get{return _ActiveRoomInside;} }
    private GameObject _ActiveRoomOutside;
    /// <summary>
    /// Property containing the active rooms outside.
    /// </summary>
    public GameObject ActiveRoom { get{return _ActiveRoomOutside;} }

    /// <summary>
    /// Property containing the total amount of rooms to create as width.
    /// </summary>
    public Vector2 RoomCount = new Vector2(8.0f, 8.0f);

#region Singleton
    private static LevelGenerator LevelGen;
    private LevelGenerator()
    {
    }
    public static LevelGenerator GetLevelGenerator()
    {
        return LevelGen;
    }
#endregion

#region lifecycle
    private void Awake()
    {
        if(LevelGen == null)
        {
            LevelGen = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if(LevelGen != this)
                Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        //Make sure ResourceManager is already loaded
        var resMgr = ResourceManager.GetResourceManager();

        if(TestMode)
            Init();
    }
#endregion

    public int ActiveRoomX
    {
        get{return _ActiveRoomX;}
        set
        {
            if(_ActiveRoomX == value)
                return;

            //Limit to +- 1
            int newX = value;
            if(newX > _ActiveRoomX)
                newX = _ActiveRoomX + 1;
            else if(newX < _ActiveRoomX)
                newX = _ActiveRoomX - 1;

            //Out of bounds check
            if(newX < 0 || newX >= RoomMap.GetLength(1))
            {
                //Mirroring check
                if(RoomMap[_ActiveRoomY, 0].Type != RoomType.Unused &&
                   RoomMap[_ActiveRoomY, RoomMap.GetLength(1) - 1].Type != RoomType.Unused)
                {
                    //Allow wrap around
                    if(newX < 0)
                        newX = RoomMap.GetLength(1) - 1;
                    else
                        newX = 0;
                }
                else //Out of bounds so don't change anything
                    return; //newX = Mathf.Clamp(newX, 0, RoomMap.GetLength(1) - 1);
            }

            if(RoomMap[_ActiveRoomY, newX].Type == RoomType.Unused)
            {
                Debug.LogError("[LevelGen] Attempted to set active room to an unused room (X: " + newX + " Y: " + _ActiveRoomY + ").");
                return;
            }

            _ActiveRoomX = newX;
        }
    }
    public int ActiveRoomY
    {
        get{return _ActiveRoomY;}
        set
        {
            if(_ActiveRoomY == value)
                return;

            //Limit to +- 1
            int newY = value;
            if(newY > _ActiveRoomY)
                newY = _ActiveRoomY + 1;
            else if(newY < _ActiveRoomY)
                newY = _ActiveRoomY - 1;

            //Out of bounds check
            if(newY < 0 || newY >= RoomMap.GetLength(0))
            {
                //Clamp to layer range
                newY = Mathf.Clamp(newY, 0, RoomMap.GetLength(0) - 1);
            }

            if(RoomMap[newY, _ActiveRoomX].Type == RoomType.Unused)
            {
                Debug.LogError("[LevelGen] Attempted to set active room to an unused room (X: " + _ActiveRoomX + " Y: " + newY + ").");
                return;
            }

            _ActiveRoomY = newY;
        }
    }

    // Start is called before the first frame update
    // void Start()
    // {
    //     if(TestMode)
    //         Init();
    // }

    /// <summary>
    /// Initializes the level generator.
    /// </summary>
    public void Init()
    {
        _ActiveRoomX = _ActiveRoomY = 0;
        _LastRoomX = _LastRoomY = -1;

        GameManager = GameManager.GetGameManager();
        ResourceManager = ResourceManager.GetResourceManager();

        LoadAllRooms();
        GenerateLevel();

        SpawnActiveRoom();
    }

    void Update()
    {
        if(TestMode)
        {
            if(Input.GetKeyDown(KeyCode.W))
            {
                ++ActiveRoomY;
                SpawnActiveRoom();
            }
            else if(Input.GetKeyDown(KeyCode.A))
            {
                --ActiveRoomX;
                SpawnActiveRoom();
            }
            else if(Input.GetKeyDown(KeyCode.S))
            {
                --ActiveRoomY;
                SpawnActiveRoom();
            }
            else if(Input.GetKeyDown(KeyCode.D))
            {
                ++ActiveRoomX;
                SpawnActiveRoom();
            }
            else if(Input.GetKeyDown(KeyCode.G))
            {
                ActiveRoomX += 5;
                SpawnActiveRoom();
            }
            else if(Input.GetKeyDown(KeyCode.R))
            {
                GenerateLevel();
                SpawnActiveRoom();
            }
        }
    }

    /// <summary>
    /// Loads all room and subroom prefabs from the ResourceManager.
    /// </summary>
    void LoadAllRooms()
    {
        //Load Normal room prefab
        if (ResourceManager.LoadAsset("Prefab/Rooms/RoomOutsideNormal") == false)
            Debug.LogError("[LevelGen] Unable to load Resources/Prefab/Rooms/RoomOutsideNormal");
        else
            Debug.Log("[LevelGen] MainRoom loaded.");

        //Load Start room prefab
        if (ResourceManager.LoadAsset("Prefab/Rooms/RoomOutsideStart") == false)
            Debug.LogError("[LevelGen] Unable to load Resources/Prefab/Rooms/RoomOutsideStart");
        else
            Debug.Log("[LevelGen] StartRoom loaded.");

        //Load Shop room prefab
        if (ResourceManager.LoadAsset("Prefab/Rooms/RoomOutsideShop") == false)
            Debug.LogError("[LevelGen] Unable to load Resources/Prefab/Rooms/RoomOutsideShop");
        else
            Debug.Log("[LevelGen] ShopRoom loaded.");

        //Load Boss room prefab
        if (ResourceManager.LoadAsset("Prefab/Rooms/RoomOutsideBoss") == false)
            Debug.LogError("[LevelGen] Unable to load Resources/Prefab/Rooms/RoomOutsideBoss");
        else
            Debug.Log("[LevelGen] BossRoom loaded.");

        //Load subroom prefabs
        int subRoomNr = 0;
        string subRoomName = "Prefab/Rooms/RoomInside" + subRoomNr;
        while (ResourceManager.LoadAsset(subRoomName))
        {
            Debug.Log("[LevelGen] " + subRoomName + " loaded.");
            subRoomName = "Prefab/Rooms/RoomInside" + (++subRoomNr);
        }
        NormalRoomInsideCount = subRoomNr;
    }

    private void InitRoomMap()
    {
        RoomMap = new RoomState[(int)RoomCount.y, (int)RoomCount.x];
        for(int y = 0; y < RoomMap.GetLength(0); ++y)
        {
            for(int x = 0; x < RoomMap.GetLength(1); ++x)
                RoomMap[y, x] = new RoomState();
        }
    }

    /// <summary>
    /// Generates the random level map.
    /// </summary>
    private void GenerateLevel()
    {
        InitRoomMap();

        //Generate boss rooom
        int bossRoomX = Random.Range(0, RoomMap.GetLength(0));
        RoomMap[7, bossRoomX].Type = RoomType.Boss; //Boss room

        //Generate start room
        _ActiveRoomX = Random.Range(0, RoomMap.GetLength(0));
        RoomMap[0, ActiveRoomX].Type = RoomType.Start; //Start room
        RoomMap[0, ActiveRoomX].Visited = true;

        //Create Space between the boss and start room
        int lastRoomIndex = ActiveRoomX;
        bool startRoom = true;
        for(int i = 0; i < RoomMap.GetLength(0) - 1; ++i)
        {
            if(startRoom) //Start room has a special value
            {
                startRoom = false;
                RoomMap[i, lastRoomIndex].Type = RoomType.Start;
            }
            else //Every other room
                RoomMap[i, lastRoomIndex].Type = RoomType.Normal;

            //Generate ranges for current layer based on the previous layers room position
            int rangeLeft = Random.Range(0, lastRoomIndex + 1); //0 -> lastRoomIndex
            int rangeRight = Random.Range(lastRoomIndex, RoomMap.GetLength(1)); //lastRoomIndex -> End of layer

            //Boss room requires special handling to make sure the previous room and the
            //boss room get connected together. So we may need to modify the ranges.
            if(i == RoomMap.GetLength(0) - 2)
            {
                if(bossRoomX < rangeLeft)
                    rangeLeft = bossRoomX;
                else if(bossRoomX > rangeRight)
                    rangeRight = bossRoomX;
            }

            //Set the rooms for this layer based on the generated range.
            for(int j = rangeLeft; j <= rangeRight; ++j)
            {
                if(RoomMap[i, j].Type == RoomType.Unused)
                    RoomMap[i, j].Type = RoomType.Normal;
            }

            //Set last room index for now this can only be a single room
            lastRoomIndex = Random.Range(rangeLeft, rangeRight + 1);
        }

        GenerateShopRoom();

        if(TestMode)
            PrintMap();
    }

    /// <summary>
    /// Generates the shop room on the level map.
    /// </summary>
    private void GenerateShopRoom()
    {
        //Generate shop room
        int shopRoomY = Random.Range(2, RoomMap.GetLength(1) - 2);
        int shopRoomX = -1;
        for(int i = 0; i < RoomMap.GetLength(1) - 1; ++i)
        {
            //Find empty room connected with a normal room
            if(RoomMap[shopRoomY, i].Type == RoomType.Unused && RoomMap[shopRoomY, i + 1].Type == RoomType.Normal)
            {
                shopRoomX = i;
                break;
            }
            else if(i > 0 && RoomMap[shopRoomY, i - 1].Type == RoomType.Normal && RoomMap[shopRoomY, i].Type == RoomType.Unused)
            {
                shopRoomX = i;
                break;
            }
        }
        if(shopRoomX != -1) //Found space
            RoomMap[shopRoomY, shopRoomX].Type = RoomType.Shop;
        else //RANDOM
            RoomMap[shopRoomY, Random.Range(0, RoomMap.GetLength(1))].Type = RoomType.Shop;
    }

    /// <summary>
    /// Spawn all rooms based on the generated room map.
    /// </summary>
    private void SpawnActiveRoom()
    {
        if(ActiveRoomX == _LastRoomX && ActiveRoomY == _LastRoomY)
        {
            Debug.LogError("Tried to regenerate same room! " + ActiveRoomX + " " + ActiveRoomY);
            return; //Do not regenerate the same room!
        }
        _LastRoomX = ActiveRoomX;
        _LastRoomY = ActiveRoomY;

        Debug.Log("SpawnActiveRoom called! " + ActiveRoomX + " " + ActiveRoomY);
        GetRoomDoors(); //Test call for Debug output

        //If old room exist remove it
        if(_ActiveRoomOutside != null)
            Destroy(_ActiveRoomOutside);
        if(_ActiveRoomInside != null)
            Destroy(_ActiveRoomInside);

        //Spawn outer Room
        switch(RoomMap[ActiveRoomY, ActiveRoomX].Type)
        {
        case RoomType.Normal:
            _ActiveRoomOutside = SpawnGameObject("Prefab/Rooms/RoomOutsideNormal");
            break;
        case RoomType.Start:
            _ActiveRoomOutside = SpawnGameObject("Prefab/Rooms/RoomOutsideStart");
            break;
        case RoomType.Shop:
            _ActiveRoomOutside = SpawnGameObject("Prefab/Rooms/RoomOutsideShop");
            break;
        case RoomType.Boss:
            _ActiveRoomOutside = SpawnGameObject("Prefab/Rooms/RoomOutsideBoss");
            break;

        case RoomType.Unused:
        default:
            break;
        }

        //Spawn inner Room
        if(NormalRoomInsideCount == -1)
            Debug.LogError("[LevelGen] NormalRoomInsideCount is -1!");
        else
        {
            if(RoomMap[ActiveRoomY, ActiveRoomX].InsideIndex == -1)
                RoomMap[ActiveRoomY, ActiveRoomX].InsideIndex = Random.Range(0, NormalRoomInsideCount);
            _ActiveRoomInside = SpawnGameObject("Prefab/Rooms/RoomInside" + RoomMap[ActiveRoomY, ActiveRoomX].InsideIndex);

            RoomType type = RoomMap[ActiveRoomY, ActiveRoomX].Type;
            if(type != RoomType.Start && type != RoomType.Shop && type != RoomType.Unused)
            {
                //TODO Spawn enemies
            }
        }
        //TODO Spawn doors for neighbouring rooms
        //TODO Change room on door collision hit + set visited to true

        RoomMap[ActiveRoomY, ActiveRoomX].Visited = true;
    }

    /// <summary>
    /// Spawns a gameobject from its given resource path.
    /// </summary>
    /// <param name="path">The path to the prefab.</param>
    private GameObject SpawnGameObject(string path)
    {
        if (ResourceManager.LoadAsset(path))
        {
            GameObject prefab = ResourceManager.GetAssetByPath<GameObject>(path);
            return Instantiate(prefab, Vector3.zero, Quaternion.identity);
        }

        return null;
    }

    /// <summary>
    /// Prints the generated room map to the debug console.
    /// </summary>
    public void PrintMap()
    {
        string roomStr = "[LevelGen] RoomMap:\n";
        for(int i = RoomMap.GetLength(0) - 1; i >= 0; --i)
        {
            for(int j = 0; j < RoomMap.GetLength(1); ++j)
            {
                if(RoomMap[i, j].Type == RoomType.Normal)
                    roomStr += " N ";
                else if(RoomMap[i, j].Type == RoomType.Boss)
                    roomStr += " B ";
                else if(RoomMap[i, j].Type == RoomType.Shop)
                    roomStr += " S ";
                else if(RoomMap[i, j].Type == RoomType.Start)
                    roomStr += " s ";
                else
                    roomStr += " U ";
            }
            roomStr += "\n";
        }
        Debug.Log(roomStr);
    }

    /// <summary>
    /// Retrieve a list of valid neighbour doors.
    /// </summary>
    public List<RoomDoors> GetRoomDoors()
    {
        List<RoomDoors> doors = new List<RoomDoors>();

        bool RowFilled = true;

        for (int i = 0; i < (int)RoomCount.x; i++)
        {
            if (RoomMap[ActiveRoomY, i].Type == RoomType.Unused)
            {
                RowFilled = false;
                break;
            }
        }

        if ((RowFilled == true && ActiveRoomX + 1 > ((int)RoomCount.x - 1)) ||
                (ActiveRoomX < RoomMap.GetLength(1) - 1 && RoomMap[ActiveRoomY, ActiveRoomX + 1].Type != RoomType.Unused))
        {
                doors.Add(RoomDoors.RightTop);
                doors.Add(RoomDoors.RightBottom);
        }

        if ((RowFilled == true && ActiveRoomX - 1 < 0) ||
            (ActiveRoomX > 0 && RoomMap[ActiveRoomY, ActiveRoomX - 1].Type != RoomType.Unused))
        {
            doors.Add(RoomDoors.LeftTop);
            doors.Add(RoomDoors.LeftBottom);
        }
        if (ActiveRoomY < RoomMap.GetLength(0) - 1 && (RoomMap[ActiveRoomY + 1 , ActiveRoomX].Type != RoomType.Unused))
        {
            doors.Add(RoomDoors.TopLeft);
            doors.Add(RoomDoors.TopRight);
        }
        if (ActiveRoomY > 0 && (RoomMap[ActiveRoomY - 1, ActiveRoomX].Type != RoomType.Unused))
        {
            doors.Add(RoomDoors.BottomLeft);
            doors.Add(RoomDoors.BottomRight);
        }
        if(TestMode)
        {
            string test = "";
            for (int i = 0; i < doors.Count; i++)
            {
                test += doors[i].ToString() + " ";
            }
            if (test.Length > 0)
            {
                Debug.Log("[LevelGen] Doors: " + test);
            }
        }
        return doors;
    }
}
