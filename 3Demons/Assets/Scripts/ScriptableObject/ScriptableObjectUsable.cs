using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item/Usables")]
public class ScriptableObjectUsable : ScriptableObject
{
    public int Normal_Key;
    public int Special_Key;
    public int Bombe;
    

    public GameObject mesh;

    public Sprite uiImage;
}


