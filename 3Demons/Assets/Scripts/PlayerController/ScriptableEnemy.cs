using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BalancingData/EnemyData")]
public class ScriptableEnemy : ScriptableObject
{
    [Header("Stats")]
    /// <summary>
    /// 
    /// </summary>
    public bool isBullet;
    /// <summary>
    /// 
    /// </summary>
    public string name;
    /// <summary>
    /// Cant get destroyed after coliding with player.
    /// </summary>
    public bool invincible;
    /// <summary>
    /// 
    /// </summary>
    public bool friendlyFireOn;
    /// <summary>
    /// 
    /// </summary>
    public int damage;
    /// <summary>
    /// 
    /// </summary>
    public int reactionTime;
    /// <summary>
    /// 
    /// </summary>
    public float forwardMovementSpeed;
    /// <summary>
    /// 
    /// </summary>
    public float backwardMovementSpeed;
    /// <summary>
    /// 
    /// </summary>
    public float topMovementSpeed;
    /// <summary>
    /// 
    /// </summary>
    public float downMovementSpeed;
    /// <summary>
    /// 
    /// </summary>
    public int maxHealth;
    /// <summary>
    /// 
    /// </summary>
    public int flyingSpeed;
    /// <summary>
    /// 
    /// </summary>
    public bool deathAnimation;
    [Header("Movement")]
    /// <summary>
    /// 
    /// </summary>
    public bool canMove;
    /// <summary>
    /// 
    /// </summary>
    public bool canFly;
    /// <summary>
    /// 
    /// </summary>
    public bool hasGravity;
    /// <summary>
    /// 
    /// </summary>
    public bool waitsAbovePlayer;
    /// <summary>
    /// 
    /// </summary>
    public bool canGoThrewWall;
    [Header("Patrolling")]
    /// <summary>
    /// 
    /// </summary>
    public bool patrollingMovement;
    /// <summary>
    /// 
    /// </summary>
    public float patrolingMovementSpeed;
    /// <summary>
    /// 
    /// </summary>
    public List<Vector3> patrollingPositions;
    /// <summary>
    /// 
    /// </summary>
    public float vektorDownRange;
    /// <summary>
    /// 
    /// </summary>
    public float vektorForwardRange;
    /// <summary>
    /// 
    /// </summary>
    public float directionChangeRange;
    [Header("Bullet Enemy")]
    /// <summary>
    /// 
    /// </summary>
    public ScriptableEnemy scriptableBullet;
    /// <summary>
    /// 
    /// </summary>
    public bool canShoot;
    /// <summary>
    /// 
    /// </summary>
    public int bulletCooldown;
}
