﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//script von Adrian
public class BulletScript : MonoBehaviour
{
    public GameObject BulletImpact;

    public void OnCollisionEnter(Collision collision)
    {
        GameObject effect = Instantiate(BulletImpact, transform.position, Quaternion.identity);
        Destroy(effect, 2f);
        Destroy(gameObject);
    }
}
