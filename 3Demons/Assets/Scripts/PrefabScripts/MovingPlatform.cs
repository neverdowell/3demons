using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//[RequireComponent(typeof(BoxCollider))] Doesnt work in this Instance
public class MovingPlatform : MonoBehaviour
{
    public Vector3 Origin;
    public Vector3 Target;
    public float speed;
    public float WaitingTime;
    float tempWaitingTime;


    void FixedUpdate()
    {
        if((Target - transform.position).magnitude <= 0.1 || Target == transform.position)
        {
            //starting Timer
            tempWaitingTime = WaitingTime;

            //Swap Vector roles
            Vector3 temp = Origin;
            Origin = Target;
            Target = temp;
        }
        if(tempWaitingTime <= 0)
        {
            //If not waiting, then Move to Target
            Move();
        }
        else
        {
            //Wait if Timer is still going
            tempWaitingTime -= Time.deltaTime;
        }
    }
    void Move()
    {
        //Move in the target direction depending on the Speed
        this.transform.position += (Target-transform.position).normalized * (speed/1000);
    }
    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.transform.SetParent(this.gameObject.transform);
    }

    private void OnCollisionExit(Collision collision)
    {
        collision.gameObject.transform.SetParent(null);
    }
}
