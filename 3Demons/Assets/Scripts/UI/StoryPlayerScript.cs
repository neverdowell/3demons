using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryPlayerScript : MonoBehaviour
{
    public CharacterController characterController;
    public int itemCount = 0;
    public Vector3 spawnPoint;
    public float speed = 8f;
    public float jumpSpeed = 8f;
    public int maxJumpCount = 2;
    public float fallingDamageSpeed = -100.0f;
    public float gravity = 10f;

    public KeyCode forwardKeyCode = KeyCode.W;
    public KeyCode backKeyCode = KeyCode.S;
    public KeyCode leftKeyCode = KeyCode.A;
    public KeyCode rightKeyCode = KeyCode.D;
    public KeyCode jumpKeyCode = KeyCode.Space;


    public Vector3 movingDirection;
    public int currentJumpCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
    }



    private void OnControllerColliderHit(ControllerColliderHit controllerColliderHit)
    {
        switch (controllerColliderHit.gameObject.tag)
        {
            case "Item":
                Destroy(controllerColliderHit.gameObject);
                itemCount++;
                break;
            case "Respawn":
                respawn();
                break;
        }
    }




    private void MovePlayer()
    {
        int x = 0;
        int z = 0;
        if (Input.GetKey(forwardKeyCode))
        {
            z++;
        }

        if (Input.GetKey(leftKeyCode))
        {
            x--;
        }

        if (Input.GetKey(backKeyCode))
        {
            z--;
        }

        if (Input.GetKey(rightKeyCode))
        {
            x++;
        }

        if (characterController.isGrounded)
        {
            currentJumpCount = 0;
            //CheckFallingDamage();
        }

        if (Input.GetKeyDown(jumpKeyCode) && currentJumpCount < maxJumpCount)
        {
            movingDirection.y = jumpSpeed;
            currentJumpCount++;
        }

        movingDirection.x = x * speed * Time.deltaTime;
        movingDirection.y -= gravity * Time.deltaTime;
        movingDirection.z = z * speed * Time.deltaTime;

        characterController.Move(movingDirection);
    }

    //private void CheckFallingDamage() {
    //    if (movingDirection.y < fallingDamageSpeed) {
    //        respawn();
    //    }
    //}

    public void respawn()
    {
        movingDirection = Vector3.zero;
        transform.position = spawnPoint;
    }
}