using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlatform : MonoBehaviour
{
    public Vector3 RotationSpeed;
 
    // Update is called once per frame
    void FixedUpdate()
    {
            this.transform.Rotate(
            RotationSpeed.x + transform.localRotation.x * Time.deltaTime,
            RotationSpeed.y + transform.localRotation.y * Time.deltaTime,
            RotationSpeed.z + transform.localRotation.z * Time.deltaTime,
	    Space.Self);
    }
}
