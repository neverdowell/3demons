using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item/Collectables")]
public class CollectiblesScriptableObject : ScriptableObject
{
    [Header("Collectibles:Werte")]
    public int LifePoints;
    public int CoinValue;
    public int EnergiePoints;
    public int AreaOfEffect;
    public int ItemValue;
    public string ItemName;
}
