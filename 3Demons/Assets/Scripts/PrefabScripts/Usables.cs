using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Usables : MonoBehaviour
{

    public ScriptableObjectUsable usableData;

    public int normal_key;
    public int special_Key;
    public int bombe;

    void Awake()
    {
        normal_key = usableData.Normal_Key;
        special_Key = usableData.Special_Key;
        bombe = usableData.Bombe;
    }
}
