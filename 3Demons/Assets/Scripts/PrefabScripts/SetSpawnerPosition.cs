using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSpawnerPosition : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 vector3;

    void Start()
    {
        vector3 = new Vector3(this.gameObject.transform.position.x,this.gameObject.transform.position.y,0);
        this.gameObject.transform.position = vector3;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
