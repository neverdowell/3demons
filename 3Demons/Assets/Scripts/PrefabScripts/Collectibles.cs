using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour
{
    public CollectiblesScriptableObject collectibleData;

    public int coinValue;
    public int lifePoints;
    public int energiePoints;
    public int areaOfEffect;
    public int itemValue;
    public string itemName;

    void Awake()
    {
        coinValue = collectibleData.CoinValue;
        lifePoints = collectibleData.LifePoints;
        energiePoints = collectibleData.EnergiePoints;
        areaOfEffect = collectibleData.AreaOfEffect;
        itemValue = collectibleData.ItemValue;
        itemName = collectibleData.ItemName;
    }
}
