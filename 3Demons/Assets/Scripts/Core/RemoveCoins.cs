using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveCoins : MonoBehaviour
{
    private static GameManager gManager;
    private static int currentCoinValue;
    private static GameObject objectToCheck;
    private static ScriptableObjectWeapon scrobjectToCheck;
    // Start is called before the first frame update
    private void Awake()
    {
        gManager = GameManager.GetGameManager();
        currentCoinValue = gManager.playerData.currentCoins;
    }

    public static void RemoveCoinsFromCurrentCoins(GameObject givenObject)
    {
        objectToCheck = givenObject;
        scrobjectToCheck = objectToCheck.GetComponentInChildren<ScriptableObjectWeapon>();
        if (scrobjectToCheck.isShopitem)
        {
            if (scrobjectToCheck.itemvalue <= currentCoinValue)
            {
                currentCoinValue -= scrobjectToCheck.itemvalue;
            }
            else
            {
                System.Console.WriteLine("Not enough Coins");
            }
        }
        gManager.playerData.currentCoins = currentCoinValue;
    }
}
