using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class BGSoundScript : MonoBehaviour
{
    public AudioMixer master;
    public AudioMixerGroup audioMixerGroup;
    private static BGSoundScript instance = null;
    public static BGSoundScript Instance
    {
        get { return instance; }
    }
    private void Awake()
    {

        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        float Volume = PlayerPrefs.GetFloat("Volume");
        master.SetFloat("MasterVolume", Volume);

    }

    private void Update()
    {

        float Volume = PlayerPrefs.GetFloat("Volume");
        if (SceneManager.GetActiveScene().name == "GameScene")
        {
            master.SetFloat("MasterVolume", -1000);
            master.SetFloat("InGame", Volume);
        }
        else if (SceneManager.GetActiveScene().name == "MenuScene")
        {
            master.SetFloat("MasterVolume", Volume);
            master.SetFloat("InGame", -1000);
        }
    }
}
