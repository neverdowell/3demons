using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(BoxCollider))]
public class DoorScript : MonoBehaviour
{
    
    public DoorStatus doorStatus;

    public string targetScene;

    // Start is called before the first frame update
    void Start()
    {
        doorStatus = DoorStatus.Locked;
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeycodeManager.GetKeyCodeByAction("interact")))
            {
                if (doorStatus == DoorStatus.Open)
                {
                    Debug.Log(targetScene);
                    SceneManager.LoadScene(targetScene);
                }
                Debug.Log("Debug:Door is " + doorStatus);
            }
        }
    }
    public void CheckDoor()
    {
        if (doorStatus == DoorStatus.Locked)
        {
            doorStatus = DoorStatus.Open;
        }
        //if (doorStatus == DoorStatus.Open)
        //{
        //    doorStatus = DoorStatus.Closed;
        //}
        Debug.Log("Debug:Door is " + doorStatus);

    }
}

public enum DoorStatus
{
    Open,
    Closed,
    Locked
}
