using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/GameData")]
public class GameData : ScriptableObject
{
    public int[] dropTable;
}
