using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item/Weapons")]
public class ScriptableObjectWeapon : ScriptableObject
{
    public GameObject weaponPrefab;//Prefab
    public string WeaponName;//Type/Name der Waffe
    public float cooldown; // cooldown wann die Waffe wieder benutzt werden kann
    public float range; //Wie weit man schiessen kann
    public float damage; //Schaden der Waffe
    public float energy; // Energie die pro Schuss verloren geht
    public string special; //special der Spezial-Waffen
    public int itemvalue;
    public bool isShopitem;



    public GameObject mesh;



    public Sprite uiImage;
    public GameObject bullet;
}
