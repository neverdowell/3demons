using UnityEngine;

public class LootScript : MonoBehaviour
{
    public GameData gameData;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void getDropItems()
    {
        int DropChance = Random.Range(1,101);
        int AnzahlItems = 0;
        if (DropChance <= gameData.dropTable[0] && DropChance > 50)
        {
            AnzahlItems = 0;
        }
        else if(DropChance <= 50 && DropChance > 30)
        {
            AnzahlItems = 1;
        }
        else if (DropChance <= 30 && DropChance > 30)
        {
            AnzahlItems = 2;
        }
        else if (DropChance <= 50 && DropChance > 30)
        {
            AnzahlItems = 3;
        }
    }
}
