using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScene_MoveBrickOnClick : MonoBehaviour
{
    [Header("Scene Change Variables")]
    #region menubtn_func
    public menubtn_func menubtn_Func;
    public enum menubtn_func
    {
        start,
        settings,
        credit,
        exit
    }
    #endregion



    private void OnMouseDown()
    {
        StartCoroutine(MoveToBrickPosition(this.transform, this.transform.position + new Vector3(0, 0, 0.5f), 2f));
    }

    public IEnumerator MoveToBrickPosition(Transform transform, Vector3 position, float timeToMove)
    {
        //move brick
        var currentPos = transform.position;
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            transform.position = Vector3.Lerp(currentPos, position, t);
            yield return null;
        }

        #region change scene
        switch (menubtn_Func)
        {
            case menubtn_func.start:
                GameManager.SceneSwitch("GameScene");
                break;
            case menubtn_func.settings:
                GameManager.SceneSwitch("SettingsScene");
                break;
            case menubtn_func.credit:
                GameManager.SceneSwitch("CreditsScene");
                break;
            case menubtn_func.exit:
                GameManager.ExitGame();
                break;
            default:
                Debug.Log("no scene selected!");
                break;
        }
        #endregion
    }

}
