using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;
using System;

//Created by pba3h19akn
public class SettingsUI : MonoBehaviour
{
    [SerializeField] private GameObject txt_res;
    public TMP_Dropdown DropdownRes;
    public AudioMixer master;
    List<string> ResList;
    Resolution[] resolution;
    public Slider sl_vol;
    Input_Manager_ke InputManager;

    void Start()
    {
        InputManager = new Input_Manager_ke();
        ResList = new List<string>();
        resolution = Screen.resolutions;
        DropdownRes.ClearOptions();

        

        foreach (var item in resolution)
        {
            ResList.Add(Convert.ToString(item));
        }
        DropdownRes.AddOptions(ResList);
    }

    void Update()
    {
    }
    public void OnClickDropRes()
    {
        Screen.SetResolution(resolution[DropdownRes.value].width, resolution[DropdownRes.value].height, true);

    }

    public void SetLevel (float SliderValue)
    {
        master.SetFloat("MasterVolume",Mathf.Log10(SliderValue)*20);
        UpdateVolume(SliderValue);
    }

    public void btn_set_input(string key_name)
    {
        Debug.Log(key_name);

        InputManager.UpdateInput(key_name);
     
    }
    public void UpdateVolume(float SliderValue)
    {
        PlayerPrefs.SetFloat("Volume", Mathf.Log10(SliderValue) * 20);
    }

    public void backToMenu()
    {
        PlayerPrefs.Save();
        GameManager.SceneSwitch("MenuScene");
    }

}
