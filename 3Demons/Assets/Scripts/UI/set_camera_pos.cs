using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class set_camera_pos : MonoBehaviour
{
    [Header("Camera")]
    public Camera camera;

    [Header("Room Prefab to adjust camera on")]
    public GameObject roomPrefab;

    [Header("Height + Depth check")]
    public GameObject upperBorder;
    public GameObject background;



    private void Start()
    {
        changeCameraPos();
    }

    private void changeCameraPos()
    {
        //camera settings
        camera.orthographic = true;
        camera.orthographicSize = 9;

        //get room data
        Vector3 roomPos = roomPrefab.transform.position;
        float roomDepth = upperBorder.transform.lossyScale.z * 1.5f;
        float roomHeight = background.transform.position.y;

        //adjust camera to room
        camera.transform.position = new Vector3(roomPos.x, roomPos.y + roomHeight, roomPos.z - (int)roomDepth);
    }
}
