using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerPrefab : MonoBehaviour
{

    private GameManager gameManager;
    //Player related variables
    [Header("Player Stats")]
    public int playerSpeed;
    public int currentHealth;

    //Ladder related variables
    private float vertical;
    public float climbSpeed;
    private bool isLadder;
    private bool isClimbing;
    public Rigidbody playerRB;


    //player ./item/collectibles
    [Header("Collect")]
    public int LifePoints;
    public int CoinValue;
    public int EnergiePoints;
    public int AreaOfEffect;
    public int ItemValue;


    //player ./item/usables
    [Header("Usables")]
    public int normalKey;
    public int specialKey;
    public int bombe;

    //Sp�ter L�schen
    [Header("Test-Header")]
    public int openedChest;
    public int openedDoor;
    public string targetScenePlayer;
    public DoorStatus doorStatusPlayer;

    private bool usablesCollectiblesKeyCodePressed = false;

    //Edited by UI. Necessary for Input Settings
    public KeyCode up = KeyCode.W;
    public KeyCode left = KeyCode.A;
    public KeyCode right = KeyCode.D;
    public KeyCode interact = KeyCode.E;

    

    void Start()
    {
        gameManager = GameManager.GetGameManager();
        //currentHealth = gameManager.playerData.maxHealth;
    }
    // Update is called once per frame
    void Update()
    {
        //Basic controls for player
        if (Input.GetKey(up))
        {
            transform.position += Vector3.up * playerSpeed * Time.deltaTime;
        }

        if (Input.GetKey(left))
        {
            transform.position += Vector3.left * playerSpeed * Time.deltaTime;
        }

        if (Input.GetKey(right))
        {
            transform.position += Vector3.right * playerSpeed * Time.deltaTime;
        }


        //Item controls for player
        if (Input.GetKeyDown(interact))
        {
            //Debug.Log("e down");
            usablesCollectiblesKeyCodePressed = true;
        } else if (Input.GetKeyUp(interact))
        {
            //Debug.Log("e up");
            usablesCollectiblesKeyCodePressed = false;
        }

        //Climbing ladder check
        vertical = Input.GetAxisRaw("Vertical");
        if (isLadder && Mathf.Abs(vertical) > 0f)
        {
            isClimbing = true;
        }

        //Climbing ladder up and down movement
        if (isClimbing)
        {
            playerRB.velocity = new Vector3(playerRB.velocity.x, vertical * climbSpeed);
        }
    }
    
    //Climbing ladder//Collecting Items OnTriggerEnter
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag.ToString())
        {
            case ("Collectibles"):
                Debug.Log(other.tag.ToString());
                Collectibles c1 = other.gameObject.GetComponent<Collectibles>();
                CoinValue += c1.coinValue;
                LifePoints += c1.lifePoints;
                EnergiePoints += c1.energiePoints;
                AreaOfEffect += c1.areaOfEffect;
                ItemValue += c1.itemValue;
                Destroy(other.gameObject);
                break;
            case ("Usables"):
                Debug.Log(other.tag.ToString());
                Usables u1 = other.gameObject.GetComponent<Usables>();
                normalKey += u1.normal_key;
                specialKey += u1.special_Key;
                bombe += u1.bombe;
                Destroy(other.gameObject);
                break;
            case ("Ladder"):
                isLadder = true;
                break;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (usablesCollectiblesKeyCodePressed)
        {
            switch (other.gameObject.tag.ToString())
            {
                case ("Door"):
                    //TODO Next Gad Siehe DoorScript.cs 29
                    doorStatusPlayer = GetComponent<DoorScript>().doorStatus;
                    targetScenePlayer = GetComponent<DoorScript>().targetScene.ToString();
                    if (specialKey > 0 && doorStatusPlayer == DoorStatus.Open)
                    {
                        //TODO
                        //Change Scene
                        Debug.Log(targetScenePlayer);
                        //SceneManager.LoadScene(targetScene);
                        GameManager.SceneSwitch(targetScenePlayer);
                        Debug.Log("Debug:Door is " + doorStatusPlayer);
                        openedDoor++;
                        other.gameObject.GetComponent<DoorScript>().CheckDoor();
                        specialKey--;
                    }
                    break;
                case ("Bomb"):
                    if (bombe > 0)
                    {
                        //TODO Explosion Script erstellen hier ausf�hren. Bombe Rigidbody
                        bombe--;
                        //other = Bombe Legen Methode
                    }
                    break;
                case ("Chest"):
                    if (normalKey > 0)
                    {
                        //other = Kiste �ffnen Methode
                        openedChest++;
                        Destroy(other.gameObject);
                        normalKey--;
                    }
                    break;
            }
        }
    }

    //Climbing ladder - TriggerExit
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ladder"))
        {
            isLadder = false;
            isClimbing = false;
        }
    }

    //Inflicting damage to player
    private void GetDamage(int dmg)
    {
        currentHealth -= dmg;
    }
}
