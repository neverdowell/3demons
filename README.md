# 3DEMONS (Arbeitstitel)

GameProject 2022 
der Klassen PBA3H19A, PBA3H19B and PBM3H19B
(c)2022 bib International College

## Getting started


## Git
### Clone
```
cd Desktop
git clone https://gitlab.com/neverdowell/3demons.git
```

### Eigenes Repo aktualisieren
```
cd Desktop/3demons
git pull
```

### Änderungen hochladen
```
cd Desktop/3demons
git add.
git commit -m "ÄNDERUNGEN ANGEBEN"
git push
```
Änderungen angeben mit:
* **ADD:** Angeben was oder welche Dateien HINZUGEFÜGT wurden
* **DEL:** Angeben was oder welche Dateien GELÖSCHT wurden
* **MOD:** Angeben was oder welche Dateien VERÄNDERT wurden
* **MOV:** Angeben was oder welche Dateien VERSCHOBEN wurden
* **REN:** Angeben was oder welche Dateien UMBENANNT wurden
