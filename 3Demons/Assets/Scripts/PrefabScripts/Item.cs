using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Anweisungen:
//Anfangs mit drop() erzeugen und Deklarieren (Ersetzt nicht den Konstruktor in der Unterklasse!)
//mit Loot() Aufsammeln

public class Item : MonoBehaviour
{
    [Header("Drop zuf�llig oder fest [ON/OFF].")]
    //Drop zuf�llig (true) oder fest (false) 
    public bool randomDrop;

    [Header("Prefab das was droppen soll")]
    //Object das etwas droppen soll z.B. ein Monster oder eine Kiste.
    public GameObject Prefab;

    [Header("Fixed Item Prefab Array")]
    //Das gedroppt werden soll.
    public GameObject[] Loot;

    private void Update()
    {
        //Testen Killt die Monster in der Prefab adi scene
        if (Input.GetKeyDown(KeyCode.O))
        {
            Drop();
            Destroy(this.gameObject);
        }
    }

    public void Drop()
    {
        switch (Prefab.tag.ToString())
        {
            case ("Monster"):
                //Zufall drop oder Festgesetzter drop.
                if (randomDrop) //Zufall drop
                {
                    Debug.Log("random" + Prefab.name);
                    //Instantiate array mit random Drop
                    RandomDrops(Loot);
                }
                else //Festgesetzter drop
                {
                    Debug.Log("fixed" + Prefab.name);
                    //Instantiate Item
                    DropFixedItem(Loot);
                }
                break;
            case ("Chest"):
                Debug.Log("1");
                GetComponent<Animation>().Play();
                Invoke("RandomDrops",1);

                //if (randomDrop) //Zufall drop
                //{
                //    Debug.Log("2");
                //    Debug.Log("random" + Prefab.name);
                //    //Instantiate array mit random Drop
                //    RandomDrops(Loot);
                //}
                //else //Festgesetzter drop
                //{
                //    Debug.Log("fixed" + Prefab.name);
                //    //Instantiate Item
                //    DropFixedItem(Loot);
                //}
                //Animation + Kiste �ffnen.
                break;
            default:
                break;
        }
    }

    public void RandomDrops(GameObject[] MoeglicherLoot = null)
    {

        if(MoeglicherLoot == null)
        {
            MoeglicherLoot=Loot;
        }
        //Random Generator 1-100 //AnzahlItemsGenerator
        int randomNumber  = Random.Range(1, 100);
        int anzahlItems = 0;


        if (randomNumber >= 50)  //50% das nichts gedroppt wird => 50-100
        {
            anzahlItems = 0 ; //Kein Drop
        }
        else if (randomNumber >= 30) //30% das 1.Item gedroppt wird. => 30-49
        {
            anzahlItems = 1;
        }
        else if (randomNumber >= 15) //15% das 2.Item gedroppt werden. => 15-20
        {
            anzahlItems = 2;

        }
        else //5% das 3.Item gedroppt werden. => 2-5
        {
            anzahlItems = 3;
        }

        for (int i = 0; i < anzahlItems; i++)
        {
            Debug.Log("random" + Prefab.name + " " + i);
            DropRandomItem(Loot[Random.Range(1, Loot.Length)]);
        }
    }

    public void DropRandomItem(GameObject gameObjectToDrop)
    {
        Instantiate(gameObjectToDrop, Prefab.transform.position, Quaternion.identity);
    }

    public void DropFixedItem(GameObject[] loot)
    {
        foreach (GameObject item in loot)
        {
            Debug.Log("fix" + Prefab.name + " ");
            Instantiate(item, Prefab.transform.position, Quaternion.identity);
        }
    }
}
