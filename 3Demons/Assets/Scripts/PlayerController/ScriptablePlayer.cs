using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BalancingData/PlayerData")]
public class ScriptablePlayer : ScriptableObject
{
    public int movementSpeed;
    public int fallSpeed;
    public int jumpHeight;
    public int maxLifepoints;
    public int maxBombs;
    public int currentCoins;
}
