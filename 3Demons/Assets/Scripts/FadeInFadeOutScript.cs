using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeInFadeOutScript : MonoBehaviour
{

	#region FIELDS
	public RawImage fadeOutUIImage; //Image welches zum "faden" genutzt wird
	public float fadeSpeed; //Geschwindigkeit des Fade-"Effekts"
	public bool FadeOut; //bool for FadeIn or FadeOut

	public enum FadeDirection
	{
		In,
		Out
	}
	#endregion

	#region MONOBHEAVIOR
	public void OnEnable()
	{
		if (FadeOut == true) //FadeOut = true, dann starte FadeOut
		{
			StartCoroutine(Fade(FadeDirection.Out));
		}
		else //FadeOut = false, dann starte FadeIn
		{
			StartCoroutine(Fade(FadeDirection.In));
		}
	}
	#endregion

	#region FADE
	private IEnumerator Fade(FadeDirection fadeDirection)
	{
		float alpha = (fadeDirection == FadeDirection.Out) ? 1 : 0;
		float fadeEndValue = (fadeDirection == FadeDirection.Out) ? 0 : 1;
		if (fadeDirection == FadeDirection.Out)
		{
			while (alpha >= fadeEndValue)
			{
				SetColorImage(ref alpha, fadeDirection); //Setze Farbe des Bildes auf volle Transparenz
				yield return null;
			}
			fadeOutUIImage.enabled = false;
		}
		else
		{
			fadeOutUIImage.enabled = true;
			while (alpha <= fadeEndValue)
			{
				SetColorImage(ref alpha, fadeDirection); //Setze Farbe des Bildes auf 0% Transparenz
				yield return null;
			}
		}
	}
	#endregion

	#region HELPERS
	private void SetColorImage(ref float alpha, FadeDirection fadeDirection)
	{
		fadeOutUIImage.color = new Color(fadeOutUIImage.color.r, fadeOutUIImage.color.g, fadeOutUIImage.color.b, alpha);
		alpha += Time.deltaTime * (1.0f / fadeSpeed) * ((fadeDirection == FadeDirection.Out) ? -1 : 1); //Bildtransparenz wird hier gesetzt 
	}
	#endregion
}