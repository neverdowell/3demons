using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeycodeManager : MonoBehaviour
{
    /// <summary>Set dictionary with default keycode values</summary>
    private static Dictionary<string, KeyCode> ActionList = new Dictionary<string, KeyCode>()
    {
        { "jump", KeyCode.W },
        { "ladderUp", KeyCode.W },
        { "ladderDown", KeyCode.S },
        { "right", KeyCode.D },
        { "left", KeyCode.A },
        { "interact", KeyCode.E },
        { "bomb", KeyCode.S },
        { "shoot", KeyCode.Mouse0 },
        { "reload", KeyCode.Mouse1 },
    };

    /// <summary>Override default key codes with custom ones from the user</summary>
    public static void LoadKeyCodes(Dictionary<string, KeyCode> newValues)
    {
        foreach (KeyValuePair<string, KeyCode> item in newValues)
        {
            ActionList[item.Key] = item.Value;
        }
    }

    /// <summary>Returns the Keycode of the requested action</summary>
    /// <exception cref="KeyNotFoundException"></exception>
    public static KeyCode GetKeyCodeByAction(string actionName)
    {
        if (!ActionList.ContainsKey(actionName))
        {
            throw new KeyNotFoundException();
        }

        return ActionList[actionName];
    }
}
