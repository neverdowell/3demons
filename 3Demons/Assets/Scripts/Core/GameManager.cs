using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    #region Objects
    [Header("Player")]
    public GameObject Player;
    public ScriptablePlayer playerData;
    public string playerTag;
    public PlayerController playerScript;
    [Header("Enemy")]
    public ScriptableEnemy[] enemyData;
    #endregion

    #region singleton
    private static GameManager instance;
    private GameManager() { }
    public static GameManager GetGameManager()
    {
        return instance;
    }

    #endregion

    #region lifecycle
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion

    private void Start()
    {
        LevelGenerator.GetLevelGenerator().Init();
        Instantiate(Player);
    }

    #region helper
    public static void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
        Application.Quit();
#endif
    }
    #endregion

    #region Scene Switch
    public static void SceneSwitch(string nextSceneName)
    {
        SceneManager.LoadSceneAsync(nextSceneName);
    }
    #endregion
}
